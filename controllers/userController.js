const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Hotel = require("../models/Hotel");

//CONTROLLER FUNCTIONS

//For Users
//User Registration Function
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return `${reqBody.email} already exists!`;
    } else if (reqBody.age < 18) {
      return `Hi! ${reqBody.firstName} you are not allowed to register. 
			You must be atleast 18 of age above to register.`;
    } else if (reqBody.password == null || reqBody.password.length < 8) {
      return "Password must be atleast 8 alphanumeric characters!";
    } else if (reqBody.password !== reqBody.confirmPassword) {
      return "Password do not match!";
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        age: reqBody.age,
        mobileNo: reqBody.mobileNo,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
      });
      return newUser.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return `${reqBody.firstName}, you are successfully registered!`;
        }
      });
    }
  });
};

// User Login Function
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return `${reqBody.email} does not exists!`;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return "Incorrect Password";
      }
    }
  });
};

//Retrieve User Information
module.exports.getMyProfile = (userData) => {
  return User.findById(userData.id).then((result) => {
    return `
		About Me 
		

		${result}

		`;
  });
};

//Update User Information
module.exports.updateMyProfile = (userData, reqBody) => {
  let updatedUserInfo = {
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    age: reqBody.age,
    mobileNo: reqBody.mobileNo,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  };

  return User.findByIdAndUpdate(userData.id, updatedUserInfo).then(
    (result, error) => {
      if (error) {
        return false;
      } else {
        return `
			User info has been updated:

			FROM:
      First Name: ${result.firstName}
      Last Name: ${result.lastName}
      Age: ${result.age}
      Mobile Number: ${result.mobileNo}
      Email:${result.email}
      Password: ********

      TO:
      First Name: ${reqBody.firstName}
      Last Name: ${reqBody.lastName}
      Age: ${reqBody.age}
      Mobile Number: ${reqBody.mobileNo}
      Email:${reqBody.email}
      Password: ********
			`;
      }
    }
  );
};

//Delete Account
module.exports.deleteMyAccount = (userData) => {
  return User.findByIdAndDelete(userData.id).then((result, error) => {
    if (error) {
      return false;
    } else {
      return `
			Account has been deleted:
      First Name: ${result.firstName}
      Last Name: ${result.lastName}
      Age: ${result.age}
      Mobile Number: ${result.mobileNo}
      Email:${result.email}
      Password: ********
			
			`;
    }
  });
};

// For Admin
// Retrieve All Accounts
module.exports.getAllAccounts = () => {
  return User.find({}).then((result) => {
    return result;
  });
};

// Assign Another Admin
module.exports.addAdmin = (data) => {
  return User.find({ email: data.email }).then((result) => {
    resultIsAdmin = result.map(({ isAdmin }) => isAdmin);
    resultFirstname = result.map(({ firstName }) => firstName);

    if (resultIsAdmin == "true") {
      return `${resultFirstname} is already an admin!`;
    } else if (resultIsAdmin == null || resultIsAdmin == "") {
      return `${data.email} does not exists!`;
    } else if (resultIsAdmin == "false") {
      let updatedRole = {
        isAdmin: true,
      };
      return User.findOneAndUpdate(data, updatedRole).then(
        (updatedRole, error) => {
          return updatedRole.save().then((result, error) => {
            if (error) {
              return error;
            } else {
              return `You successfully added ${result.firstName} as an admin!`;
            }
          });
        }
      );
    }
  });
};

// Remove Admin
module.exports.removeAdmin = (data, userData) => {
  return User.findById(userData.id).then((user) => {
    if (user.email == data.email) {
      return `You cannot remove yourself as an admin ${user.firstName}`;
    } else {
      return User.find({ email: data.email }).then((result) => {
        resultIsAdmin = result.map(({ isAdmin }) => isAdmin);
        resultFirstname = result.map(({ firstName }) => firstName);

        if (resultIsAdmin == "false") {
          return `${resultFirstname} is not an admin!`;
        } else if (resultIsAdmin == null || resultIsAdmin == "") {
          return `${data.email} does not exists!`;
        } else if (resultIsAdmin == "true") {
          let updatedRole = {
            isAdmin: false,
          };
          return User.findOneAndUpdate(data, updatedRole).then(
            (updatedRole, error) => {
              return updatedRole.save().then((result, error) => {
                if (error) {
                  return error;
                } else {
                  return `You successfully removed ${result.firstName} as an admin!`;
                }
              });
            }
          );
        }
      });
    }
  });
};

//Retrieve authenticated users booking
module.exports.getMyBooking = (userData) => {
  return User.findById(userData.id).then((result) => {
    if (result.bookedHotel == null || result.bookedHotel == "") {
      return "Nothing Found";
    } else {
      return `
      Booked Hotel: 
      ${result.bookedHotel}

      Total Amount: PHP ${result.totalAmount}
      `;
    }
  });
};

//Retrieve all booking
module.exports.getAllBooking = () => {
  return Hotel.find({}).then((result) => {
    function getHotel(item) {
      return [item.name, item.userBook];
    }

    return result.map(getHotel);
  });
};

//Book Hotel
module.exports.bookHotel = (userData, data) => {
  return User.findById(userData.id).then((user) => {
    return Hotel.findById(data.hotelId).then((hotel) => {
      if (data.rooms > hotel.rooms) {
        return `Not enough room(s) | ${hotel.rooms} room(s) available!`;
      } else if (hotel.isActive == false) {
        return `${hotel.name} is not available!`;
      } else {
        let subtotalAmount = data.rooms * hotel.price;

        user.bookedHotel.push({
          hotelId: hotel.id,
          hotelName: hotel.name,
          hotelAddress: `${hotel.address.street}, ${hotel.address.baranggay}, ${hotel.address.city}, ${hotel.address.province}, ${hotel.address.country}`,
          roomQuantity: data.rooms,
          subtotal: subtotalAmount,
        });

        return user.save().then((user) => {
          hotel.userBook.push({
            userId: userData.id,
            userName: `${user.firstName} ${user.lastName}`,
            bookId: data.bookId,
          });

          return hotel.save().then((hotel) => {
            let newRooms = hotel.rooms - data.rooms;
            let updatedRooms = {
              rooms: newRooms,
            };

            return Hotel.findByIdAndUpdate(data.hotelId, updatedRooms).then(
              (result) => {
                if (newRooms == 0) {
                  let noRooms = {
                    isActive: false,
                  };

                  return Hotel.findByIdAndUpdate(data.hotelId, noRooms).then(
                    (result) => {
                      return User.aggregate([
                        { $match: { email: userData.email } },
                        { $unwind: "$bookedHotel" },
                        {
                          $group: {
                            _id: "$_id",
                            totalAmount: { $sum: "$bookedHotel.subtotal" },
                          },
                        },
                        { $project: { _id: 0 } },
                      ]).then((data) => {
                        let dataMap = data.map(
                          ({ totalAmount }) => totalAmount
                        );
                        let checkTotal = {
                          totalAmount: parseInt(dataMap),
                        };

                        return User.findByIdAndUpdate(
                          userData.id,
                          checkTotal
                        ).then((checkTotal) => {
                          return `Booking to ${hotel.name} is successful `;
                        });
                      });
                    }
                  );
                } else {
                  return User.aggregate([
                    { $match: { email: userData.email } },
                    { $unwind: "$bookedHotel" },
                    {
                      $group: {
                        _id: "$_id",
                        totalAmount: { $sum: "$bookedHotel.subtotal" },
                      },
                    },
                    { $project: { _id: 0 } },
                  ]).then((data) => {
                    let dataMap = data.map(({ totalAmount }) => totalAmount);
                    let checkTotal = {
                      totalAmount: parseInt(dataMap),
                    };

                    return User.findByIdAndUpdate(userData.id, checkTotal).then(
                      (checkTotal) => {
                        return `Booking to ${hotel.name} is successful `;
                      }
                    );
                  });
                }
              }
            );
          });
        });
      }
    });
  });
};

// Add to cart
module.exports.addtoCart = (userData, data) => {
  return User.findById(userData.id).then((user) => {
    return Hotel.findById(data.hotelId).then((hotel) => {
      if (data.rooms > hotel.rooms) {
        return `Not enough room(s) | ${hotel.rooms} room(s) available!`;
      } else if (hotel.isActive == false) {
        return `${hotel.name} is not available!`;
      } else {
        let subtotalAmount = data.rooms * hotel.price;

        user.cart.push({
          hotelId: hotel.id,
          hotelName: hotel.name,
          hotelAddress: `${hotel.address.street}, ${hotel.address.baranggay}, ${hotel.address.city}, ${hotel.address.province}, ${hotel.address.country}`,
          roomQuantity: data.rooms,
          subtotal: subtotalAmount,
        });

        return user.save().then((user) => {
          return `${hotel.name} successfully added to cart! `;
        });
      }
    });
  });
};

//Test Function for aggregate
module.exports.total = (userData) => {
  return User.aggregate([
    { $match: { email: userData.email } },
    { $unwind: "$bookedHotel" },
    { $group: { _id: "$_id", totalAmount: { $sum: "$bookedHotel.subtotal" } } },
    { $project: { _id: 0 } },
  ]).then((data) => {
    let datamap = data.map(({ totalAmount }) => totalAmount);
    console.log(datamap);
    console.log(userData.email);
    console.log(data.cart);

    return data;
  });
};
