const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Hotel = require("../models/Hotel");

//CONTROLLER FUNCTIONS

//Add Hotel
module.exports.addHotel = (reqBody) => {
  let hotelName = reqBody.name;
  let street = reqBody.address.street;
  let baranggay = reqBody.address.baranggay;
  let city = reqBody.address.city;
  let province = reqBody.address.province;
  let country = reqBody.address.country;
  let name = new RegExp(["^", hotelName, "$"].join(""), "i");
  let addStreet = new RegExp(["^", street, "$"].join(""), "i");
  let addBaranggay = new RegExp(["^", baranggay, "$"].join(""), "i");
  let addCity = new RegExp(["^", city, "$"].join(""), "i");
  let addProvince = new RegExp(["^", province, "$"].join(""), "i");
  let addCountry = new RegExp(["^", country, "$"].join(""), "i");
  return Hotel.find({
    $and: [
      { name: name },
      { "address.street": addStreet },
      { "address.baranggay": addBaranggay },
      { "address.city": addCity },
      { "address.province": addProvince },
      { "address.country": addCountry },
    ],
  }).then((result) => {
    console.log(result);
    if (result == "") {
      let newHotel = new Hotel({
        name: reqBody.name,
        address: {
          street: reqBody.address.street,
          baranggay: reqBody.address.baranggay,
          city: reqBody.address.city,
          province: reqBody.address.province,
          country: reqBody.address.country,
        },
        description: reqBody.description,
        price: reqBody.price,
        rooms: reqBody.rooms,
      });
      return newHotel.save().then((hotel, error) => {
        if (error) {
          return false;
        } else {
          return `${reqBody.name} | ${reqBody.address.street}, ${reqBody.address.baranggay}, ${reqBody.address.city}, ${reqBody.address.province}, ${reqBody.address.country} has been added!`;
        }
      });
      // return result
    } else {
      return `${reqBody.name} | ${reqBody.address.street}, ${reqBody.address.baranggay}, ${reqBody.address.city}, ${reqBody.address.province}, ${reqBody.address.country} already exists!`;
    }
  });
};

//Update Hotel Info (admin)
module.exports.updateHotel = (reqParams, reqBody) => {
  let updatedHotelInfo = {
    name: reqBody.name,
    address: {
      street: reqBody.address.street,
      baranggay: reqBody.address.baranggay,
      city: reqBody.address.city,
      province: reqBody.address.province,
      country: reqBody.address.country,
    },
    description: reqBody.description,
    price: reqBody.price,
    rooms: reqBody.rooms,
  };

  return Hotel.findByIdAndUpdate(reqParams.hotelId, updatedHotelInfo).then(
    (updatedHotelInfo, error) => {
      if (error) {
        return false;
      } else {
        return `
			Hotel has been updated!

			From:
			Hotel Name: ${updatedHotelInfo.name}
			Address: ${updatedHotelInfo.address.street}, ${updatedHotelInfo.address.baranggay}, ${updatedHotelInfo.address.city}, ${updatedHotelInfo.address.province}, ${updatedHotelInfo.address.country}.
			Description: ${updatedHotelInfo.description}
			Price: ${updatedHotelInfo.price}
			Rooms Available: ${updatedHotelInfo.rooms}

			To:
			Hotel Name: ${reqBody.name}
			Address: ${reqBody.address.street}, ${reqBody.address.baranggay}, ${reqBody.address.city}, ${reqBody.address.province}, ${reqBody.address.country}.
			Description: ${reqBody.description}
			Price: ${reqBody.price}
			Rooms Available: ${reqBody.rooms}

			`;
      }
    }
  );
};

// Archive/Unarchive Hotel (admin only)
module.exports.archiveHotel = (reqParams) => {
  let archivedHotelStatus = {
    isActive: reqParams.isActive,
  };

  return Hotel.findByIdAndUpdate(reqParams.hotelId, archivedHotelStatus).then(
    (status, error) => {
      console.log(`Initial Status: ${status.isActive}`);
      if (error) {
        return false;
      } else if (status.isActive) {
        // Archive hotel
        let newStatus = {
          isActive: false,
        };
        return Hotel.findByIdAndUpdate(reqParams.hotelId, newStatus).then(
          (newStatus, error) => {
            if (error) {
              return false;
            } else {
              return "Hotel has been deactivated!";
            }
          }
        );
      } else if (status.isActive == false) {
        // Unarchive hotel
        let newStatus = {
          isActive: true,
        };
        return Hotel.findByIdAndUpdate(reqParams.hotelId, newStatus).then(
          (newStatus, error) => {
            if (error) {
              return false;
            } else {
              return "Hotel has been activated!";
            }
          }
        );
      }
    }
  );
};

// Retrieve all hotel (Admin only)
module.exports.getAllHotels = () => {
  return Hotel.find({}).then((result) => {
    return `
		List of Hotels:

		${result}
		`;
  });
};

// Retrieve all active hotel (users)
module.exports.getAllActive = () => {
  return Hotel.find({ isActive: true }).then((result) => {
    return `
		List of Active Hotels:

		${result}
		`;
  });
};

// Retrieve a single Hotel (user)
module.exports.getHotelUser = (reqParams) => {
  return Hotel.findById(reqParams.hotelId).then((result, error) => {
    if (error) {
      return false;
    } else if (result.isActive) {
      return `
			Hotel: 

			${result}
			`;
    } else {
      return "Hotel is not available!";
    }
  });
};

// Retrieve a single hotel (admin)
module.exports.getHotelAdmin = (reqParams) => {
  return Hotel.findById(reqParams.hotelId).then((result, error) => {
    if (error) {
      return false;
    } else {
      return result;
    }
  });
};

//Delete Hotel
module.exports.deleteHotel = (reqParams) => {
  return Hotel.findByIdAndDelete(reqParams.hotelId).then(
    (result, error) => {
      if (error) {
        return false;
      } else {
        return `
      Hotel has been deleted:

      ${result}
      `;
      }
    }
  );
};
