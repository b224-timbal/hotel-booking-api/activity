// Basic Express Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const hotelRoutes = require("./routes/hotelRoutes");
mongoose.set("strictQuery", true);

const app = express();
const port = process.env.PORT || 4000;

//Mongoose Connection Setup
mongoose.connect(
  "mongodb+srv://Ronie2022:admin123@batch224-timbal.dlwoo0h.mongodb.net/hotelBooking-API?retryWrites=true&w=majority",

  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", () => console.error("Connection Error!"));
db.once("open", () => console.log("Connected to MongoDB!"));

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Main URI
app.use("/users", userRoutes);
app.use("/hotels", hotelRoutes);

app.listen(port, () => {
  console.log(`HOTEL BOOKING API is now running at port: ${port}`);
});
