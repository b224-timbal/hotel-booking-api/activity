const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//	ROUTES

// For Users
//Register Account Route
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//User Login Route
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Retrieve User Information
router.get("/myProfile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getMyProfile({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

//Update User Information
router.put("/updateProfile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .updateMyProfile({ id: userData.id }, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Delete Account
router.delete("/deleteAccount", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .deleteMyAccount({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// For Admin
//Retrieve all user information
router.get("/allAccounts", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    userController
      .getAllAccounts()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "Failed" });
  }
});

// Assign Another Admin
router.put("/addAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    let data = {
      email: req.body.email,
    };
    userController
      .addAdmin(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Remove Admin
router.put("/removeAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    let data = {
      email: req.body.email,
    };
    userController
      .removeAdmin(data, userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Retrieve authenticated users booking
router.get("/myBooking", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    userController
      .getMyBooking({ id: userData.id })
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Retrieve all booking
router.get("/allBooking", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .getAllBooking()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Book Hotel route
router.put("/bookNow", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    let data = {
      hotelId: req.body.hotelId,
      rooms: req.body.rooms,
      bookId: req.body.bookId,
    };
    userController
      .bookHotel(userData, data)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Add to Cart
router.put("/addtoCart", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    let data = {
      hotelId: req.body.hotelId,
      rooms: req.body.rooms,
    };
    userController
      .addtoCart(userData, data)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Test route for aggregate
router.put("/aggregate", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    userController
      .total(userData)
      .then((resultFromController) => res.send(resultFromController));
  }
});
module.exports = router;
