const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const hotelController = require("../controllers/hotelController");
const auth = require("../auth");

// ROUTES

// Add Hotel
router.post("/addHotel", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    hotelController
      .addHotel(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "Failed" });
  }
});

//Update Hotel Info (admin)
router.put("/update/:hotelId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    hotelController
      .updateHotel(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" }); //Not Admin
  }
});

//Archive Hotel (admin only)
router.put("/archive/:hotelId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    hotelController
      .archiveHotel(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

// Retrieve all hotels route (admin only)
router.get("/allHotels", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    hotelController
      .getAllHotels()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send({ auth: "failed" });
  }
});

//Retrieve all active hotels route (users)
router.get("/allActive", (req, res) => {
  hotelController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

//Retrieve a single product
router.get("/:hotelId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    hotelController
      .getHotelAdmin(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    hotelController
      .getHotelUser(req.params)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Delete Hotel
router.delete("/delete/:hotelId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    hotelController
      .deleteHotel(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" }); //Not Admin
  }
});
module.exports = router;
