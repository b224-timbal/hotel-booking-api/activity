const mongoose = require("mongoose");
const hotelSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Hotel name is required!"],
  },
  address: {
    street: {
      type: String,
      required: [true, "Hotel address is required!"],
    },
    baranggay: {
      type: String,
      required: [true, "Hotel address is required!"],
    },
    city: {
      type: String,
      required: [true, "Hotel address is required!"],
    },
    province: {
      type: String,
      required: [true, "Hotel address is required!"],
    },
    country: {
      type: String,
      required: [true, "Hotel address is required!"],
    },
  },
  description: {
    type: String,
    required: [true, "Hotel description is required!"],
  },
  price: {
    type: Number,
    required: [true, "Price is required!"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  rooms: {
    type: Number,
    required: [true, "Rooms is required!"],
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  userBook: [
    {
      userId: {
        type: String,
        required: [true],
      },
      userName: {
        type: String,
        required: [true],
      },
      bookId: {
        type: String,
        required: [false],
      },
    },
  ],
});

module.exports = mongoose.model("Hotel", hotelSchema);
