const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First Name is required!"],
  },
  lastName: {
    type: String,
    required: [true, "Last Name is required!"],
  },
  age: {
    type: Number,
    required: [true, "Age is required!"],
  },
  mobileNo: {
    type: Number,
    required: [true, "Mobile number is required!"],
  },
  email: {
    type: String,
    lowercase: true,
    required: [true, "Email is required!"],
  },
  password: {
    type: String,
    required: [true, "Password is required!"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  cart: [
    {
      hotelId: {
        type: String,
        required: [true, "Hotel ID is required!"],
      },
      hotelName: {
        type: String,
        required: true,
      },
      hotelAddress: {
        type: String,
        required: true,
      },
      roomQuantity: {
        type: Number,
        default: 1,
      },
      subtotal: {
        type: Number,
        required: true,
      },
    },
  ],
  bookedHotel: [
    {
      hotelId: {
        type: String,
        required: [true, "Hotel ID is required!"],
      },
      hotelName: {
        type: String,
        required: true,
      },
      hotelAddress: {
        type: String,
        required: true,
      },
      roomQuantity: {
        type: Number,
        default: 1,
      },
      subtotal: {
        type: Number,
        required: true,
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("User", userSchema);
